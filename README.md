### Aprašymas ###

Pixel Painter - tai piešimo programa, kuri yra panaši į "Komenskio Logo" programą.
Vartotojas komandine eilute rašo tekstą, kurį programa analizuoja ir atitinkamai vykdo komandas. Komandomis yra kontroliuojamas veikėjas, kuris piešia figūras ir linijas.

Galimos komandos:

* Draw (nupiešia figūrą)
* Speak (parašo tekstą veikėjo pozicijoj)
* Walk (pakeičia veikėjo poziciją nubrėžiant liniją)
* Color (parenkama piešimo pirminė ir antrinė spalvos)
* Thickness (parenkamas linijos storis)

Žaidimas turi tris piešimo rėžimus:

* Paprastą (laisvai pasirenkama piešinio spalva ir linijos storis)
* Brėžinio (piešinio spalva yra mėlyna, linijos splava yra balta, o storis yra minimalus)
* Juodą-baltą (piešinio spalva yra balta, o linijos spalva yra juoda)


### Klasių diagrama ###

## LD4 ##
![diagram.png](https://bitbucket.org/repo/aj8r88/images/2142031308-diagram.png)

## LD3 ##
![diagram.png](https://bitbucket.org/repo/aj8r88/images/4112289669-diagram.png)

## LD2 ##
![diagram.png](https://bitbucket.org/repo/aj8r88/images/1604965772-diagram.png)

## LD1 ##
![Paint pro deluxe v4.png](https://bitbucket.org/repo/aj8r88/images/3679205761-Paint%20pro%20deluxe%20v4.png)