package md.pixelpainter.game.command;

import md.pixelpainter.game.command.impl.DrawShapeCommand;
import md.pixelpainter.game.command.impl.NullCommand;
import md.pixelpainter.game.command.impl.SpeakCommand;
import md.pixelpainter.game.command.impl.WalkCommand;

import java.util.HashMap;
import java.util.Map;

public class CommandRegistry {

    private static CommandRegistry instance;

    private final Map<String, Command> commands = new HashMap<>();

    private CommandRegistry() {
        commands.put("walk", new WalkCommand());
        commands.put("speak", new SpeakCommand());
        commands.put("draw", new DrawShapeCommand());
	    commands.put("null", new NullCommand());
    }

    public Command getCommand(String name) {
        if (!commands.containsKey(name)) {
            return commands.get("null");
        }
        return commands.get(name);
    }

    public static CommandRegistry getInstance() {
        if (instance == null) {
            instance = new CommandRegistry();
        }
        return instance;
    }

}