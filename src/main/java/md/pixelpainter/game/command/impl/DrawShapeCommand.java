package md.pixelpainter.game.command.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.Game;
import md.pixelpainter.game.character.Character;
import md.pixelpainter.game.command.Command;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.decorator.DashedOutlineShapeDecorator;
import md.pixelpainter.game.shape.decorator.DottedOutlineShapeDecorator;

public class DrawShapeCommand implements Command {

    @Override
    public void execute(Character character, String... arguments) {
        Size size = new Size(Integer.parseInt(arguments[1]), Integer.parseInt(arguments[2]));
        Color fillColor = arguments.length >= 4 ? Color.web(arguments[3]) : Color.WHEAT;
        Color outlineColor = arguments.length >= 5 ? Color.web(arguments[4]) : Color.DARKBLUE;
        int thickness = arguments.length >= 6 ? Integer.parseInt(arguments[5]) : 1;

	    Shape shape = null;

	    String[] shapeArgs = arguments[0].toLowerCase().split("-");

        switch (shapeArgs[0]) {
            case "ellipse":
                shape = Game.getMode().getShapeFactory().createEllipse(size, fillColor, outlineColor, thickness);
                break;
            case "diamond":
                shape = Game.getMode().getShapeFactory().createDiamond(size, fillColor, outlineColor, thickness);
                break;
            case "rectangle":
                shape = Game.getMode().getShapeFactory().createRectangle(size, fillColor, outlineColor, thickness);
                break;
	        case "bubble":
	        	shape = Game.getMode().getShapeFactory().createBubble(size);
	        	break;
        }

        if (shapeArgs.length > 1) {
        	switch (shapeArgs[1]) {
		        case "dashed":
		        	shape = new DashedOutlineShapeDecorator(shape);
		        	break;
		        case "dotted":
		        	shape = new DottedOutlineShapeDecorator(shape);
		        	break;
	        }
        }

        System.out.println("Drawing shape: " + shape);
    }

    @Override
    public boolean isValid(String... arguments) {
        if (arguments.length <= 2) {
            return false;
        }
        if (arguments.length >= 3) {
            try {
                Integer.parseInt(arguments[1]);
                Integer.parseInt(arguments[2]);
            } catch (NumberFormatException ex) {
                return false;
            }
        }
        for (int i = 5; i >= 4; i--) {
            if (arguments.length >= i && (!arguments[i-1].startsWith("#") || arguments[i-1].length() != 7)) {
                return false;
            }
        }
        if (arguments.length >= 6) {
            try {
                Integer.parseInt(arguments[5]);
            } catch (NumberFormatException ex) {
                return false;
            }
        }
        return true;
    }
}