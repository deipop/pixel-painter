package md.pixelpainter.game.command.impl;

import md.pixelpainter.game.character.Character;
import md.pixelpainter.game.command.Command;
import md.pixelpainter.game.util.logger.Logger;

public class NullCommand implements Command {

	@Override
	public void execute(Character character, String... arguments) {
		Logger.logError("Command not found.");
	}

	@Override
	public boolean isValid(String... arguments) {
		return true;
	}

}
