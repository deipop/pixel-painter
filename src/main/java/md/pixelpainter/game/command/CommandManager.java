package md.pixelpainter.game.command;

import md.pixelpainter.game.character.Character;
import md.pixelpainter.game.util.logger.Logger;

public class CommandManager {

    private final Character character;

    public CommandManager(Character character) {
        this.character = character;
    }

    public void parse(String input) {
        String[] commandWithArguments = input.split(" ");

        String commandName = commandWithArguments[0];
        String[] arguments = new String[commandWithArguments.length - 1];
        System.arraycopy(commandWithArguments, 1, arguments, 0, arguments.length);
        Command command = CommandRegistry.getInstance().getCommand(commandName);
        if (command.isValid(arguments)) {
            command.execute(character, arguments);
        } else {
            Logger.logError("Incorrect arguments.");
        }
    }

}