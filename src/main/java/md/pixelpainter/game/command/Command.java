package md.pixelpainter.game.command;

import md.pixelpainter.game.character.Character;

public interface Command {

    void execute(Character character, String... arguments);

    boolean isValid(String... arguments);

}
