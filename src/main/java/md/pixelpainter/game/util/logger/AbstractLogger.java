package md.pixelpainter.game.util.logger;

public abstract class AbstractLogger {

	private LogLevel level;

	private AbstractLogger nextLogger;

	public AbstractLogger(LogLevel level) {
		this.level = level;
	}

	public void setNextLogger(AbstractLogger nextLogger) {
		this.nextLogger = nextLogger;
	}

	public void log(LogLevel level, String message) {
		if (this.level == level) {
			print(message);
			return;
		}

		if (nextLogger != null) {
			nextLogger.log(level, message);
		}
	}

	protected abstract void print(String message);

}