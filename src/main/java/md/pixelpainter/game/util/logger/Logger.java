package md.pixelpainter.game.util.logger;

import md.pixelpainter.game.util.logger.impl.ErrorLogger;
import md.pixelpainter.game.util.logger.impl.InfoLogger;

public class Logger {

    private static AbstractLogger getChain() {
    	AbstractLogger errorLogger = new ErrorLogger(LogLevel.ERROR);
	    AbstractLogger infoLogger = new InfoLogger(LogLevel.INFO);

	    infoLogger.setNextLogger(errorLogger);

	    return infoLogger;
    }

    public static void log() {
    	System.out.println();
    }

	public static void logInfo(String message) {
		getChain().log(LogLevel.INFO, message);
	}

    public static void logError(String message) {
		getChain().log(LogLevel.ERROR, message);
	}

}