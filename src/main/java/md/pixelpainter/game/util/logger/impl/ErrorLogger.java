package md.pixelpainter.game.util.logger.impl;

import md.pixelpainter.game.util.logger.AbstractLogger;
import md.pixelpainter.game.util.logger.LogLevel;

public class ErrorLogger extends AbstractLogger {

	public ErrorLogger(LogLevel level) {
		super(level);
	}

	@Override
	protected void print(String message) {
		System.out.println("[ERROR]: " + message);
	}

}