package md.pixelpainter.game.util.logger.impl;

import md.pixelpainter.game.util.logger.AbstractLogger;
import md.pixelpainter.game.util.logger.LogLevel;

public class InfoLogger extends AbstractLogger {

	public InfoLogger(LogLevel level) {
		super(level);
	}

	@Override
	protected void print(String message) {
		System.out.println("[INFO]: " + message);
	}

}