package md.pixelpainter.game.character;

public class Character {

    private final Position position;

    public Character(Position position) {
    	this.position = position;
    }

    public Position getPosition() {
        return position;
    }

	@Override
	public String toString() {
		return "Character{" +
				"position=" + position +
				'}';
	}

}