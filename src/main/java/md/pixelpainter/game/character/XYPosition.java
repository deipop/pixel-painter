package md.pixelpainter.game.character;

public class XYPosition implements Position {

    private static final int DEFAULT_X = 0;
    private static final int DEFAULT_Y = 0;

    private int x;
    private int y;

    public XYPosition() {
        this(DEFAULT_X, DEFAULT_Y);
    }

    public XYPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void offsetX(int offset) {
        offset(offset, 0, 0);
    }

    public void offsetY(int offset) {
        offset(0, offset, 0);
    }

	@Override
	public void offsetZ(int offset) {

	}

	@Override
	public void offset(int xOffset, int yOffset, int zOffset) {
		x += xOffset;
		y += yOffset;
	}

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

	@Override
	public int getZ() {
		return 0;
	}

	@Override
	public String toString() {
		return "XYPosition{" +
				"x=" + x +
				", y=" + y +
				'}';
	}

}