package md.pixelpainter.game.character;

public interface Position {

	void offsetX(int offset);
	void offsetY(int offset);
	void offsetZ(int offset);
	void offset(int xOffset, int yOffset, int zOffset);

	int getX();
	int getY();
	int getZ();

}