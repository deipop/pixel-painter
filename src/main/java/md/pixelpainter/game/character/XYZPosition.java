package md.pixelpainter.game.character;

public class XYZPosition implements Position {

    private static final int DEFAULT_X = 0;
    private static final int DEFAULT_Y = 0;
	private static final int DEFAULT_Z = 0;

    private int x;
    private int y;
	private int z;

    public XYZPosition() {
        this(DEFAULT_X, DEFAULT_Y, DEFAULT_Z);
    }

    public XYZPosition(int x, int y, int z) {
        this.x = x;
        this.y = y;
	    this.z = z;
    }

    public void offsetX(int offset) {
        offset(offset, 0, 0);
    }

    public void offsetY(int offset) {
        offset(0, offset, 0);
    }

	@Override
	public void offsetZ(int offset) {
		offset(0, 0, offset);
	}

	@Override
	public void offset(int xOffset, int yOffset, int zOffset) {
		x += xOffset;
		y += yOffset;
		z += zOffset;
	}

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

	@Override
	public int getZ() {
		return z;
	}

	@Override
	public String toString() {
		return "XYZPosition{" +
				"x=" + x +
				", y=" + y +
				", z=" + z +
				'}';
	}

}