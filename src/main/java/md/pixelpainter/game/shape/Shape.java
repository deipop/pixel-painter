package md.pixelpainter.game.shape;

import javafx.scene.paint.Color;

public interface Shape {

    Size getSize();

    Color getFillColor();

    Color getOutlineColor();

    int getThickness();

}