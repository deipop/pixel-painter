package md.pixelpainter.game.shape.criteria;

import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public class ThicknessCriteria implements Criteria {

    private final int thicknessWidthOffset;
    private final int thicknessHeightOffset;

    public ThicknessCriteria(int thicknessWidthOffset, int thicknessHeightOffset) {
        this.thicknessWidthOffset = thicknessWidthOffset;
        this.thicknessHeightOffset = thicknessHeightOffset;
    }

    @Override
    public boolean meets(Shape shape) {
        int thickness = shape.getThickness();
        Size size = shape.getSize();
        return thickness + thicknessWidthOffset <= size.getWidth() / 2
                && thickness + thicknessHeightOffset <= size.getHeight() / 2;
    }
}
