package md.pixelpainter.game.shape.criteria;

import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public class SizeCriteria implements Criteria {

    private final int height;
    private final int width;

    public SizeCriteria(int height, int width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public boolean meets(Shape shape) {
        Size size = shape.getSize();
        return size.getHeight() >= height && size.getWidth() >= width;
    }
}
