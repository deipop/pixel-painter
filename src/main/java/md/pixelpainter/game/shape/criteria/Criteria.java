package md.pixelpainter.game.shape.criteria;

import md.pixelpainter.game.shape.Shape;

public interface Criteria {

    boolean meets(Shape shape);
}
