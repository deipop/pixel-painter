package md.pixelpainter.game.shape.impl.diamond;

import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.BlueprintShape;

public class BlueprintDiamond extends BlueprintShape {

    public BlueprintDiamond(Size size) {
        super(size);
    }

	@Override
	public String toString() {
		return "BlueprintDiamond{" +
				"size=" + getSize() +
				'}';
	}

}
