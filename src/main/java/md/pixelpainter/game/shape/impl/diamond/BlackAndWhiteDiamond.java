package md.pixelpainter.game.shape.impl.diamond;

import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.BlackAndWhiteShape;

public class BlackAndWhiteDiamond extends BlackAndWhiteShape {

    public BlackAndWhiteDiamond(int thickness, Size size) {
        super(thickness, size);
    }

	@Override
	public String toString() {
		return "BlackAndWhiteDiamond{" +
				"thickness=" + getThickness() +
				", size=" + getSize() +
				'}';
	}

}
