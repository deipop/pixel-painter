package md.pixelpainter.game.shape.impl.rectangle;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.DefaultShape;

public class DefaultRectangle extends DefaultShape {

    public DefaultRectangle(Size size, Color fillColor, Color outlineColor, int thickness) {
        super(size, fillColor, outlineColor, thickness);
    }

	@Override
	public String toString() {
		return "DefaultRectangle{" +
				"size=" + getSize() +
				", fillColor=" + getFillColor() +
				", outlineColor=" + getOutlineColor() +
				", thickness=" + getThickness() +
				'}';
	}
}
