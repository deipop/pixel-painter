package md.pixelpainter.game.shape.impl.rectangle;

import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.BlueprintShape;

public class BlueprintRectangle extends BlueprintShape {

    public BlueprintRectangle(Size size) {
        super(size);
    }

	@Override
	public String toString() {
		return "BlueprintRectangle{" +
				"size=" + getSize() +
				'}';
	}

}
