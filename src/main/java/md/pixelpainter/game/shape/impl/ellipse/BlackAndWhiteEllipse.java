package md.pixelpainter.game.shape.impl.ellipse;

import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.BlackAndWhiteShape;

public class BlackAndWhiteEllipse extends BlackAndWhiteShape {

    public BlackAndWhiteEllipse(int thickness, Size size) {
        super(thickness, size);
    }

	@Override
	public String toString() {
		return "BlackAndWhiteEllipse{" +
				"thickness=" + getThickness() +
				", size=" + getSize() +
				'}';
	}

}
