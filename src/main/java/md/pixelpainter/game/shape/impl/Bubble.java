package md.pixelpainter.game.shape.impl;

public class Bubble {

	private final int[] size;

	public Bubble(int[] size) {
		this.size = size;
	}

	public int[] getSize() {
		return size;
	}

	@Override
	public String toString() {
		return "Bubble{" +
				"size=" + size[0] + ":" + size[1] +
				'}';
	}

}