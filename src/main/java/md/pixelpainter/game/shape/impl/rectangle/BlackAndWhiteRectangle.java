package md.pixelpainter.game.shape.impl.rectangle;

import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.BlackAndWhiteShape;

public class BlackAndWhiteRectangle extends BlackAndWhiteShape {

    public BlackAndWhiteRectangle(int thickness, Size size) {
        super(thickness, size);
    }

	@Override
	public String toString() {
		return "BlackAndWhiteRectangle{" +
				"thickness=" + getThickness() +
				", size=" + getSize() +
				'}';
	}

}
