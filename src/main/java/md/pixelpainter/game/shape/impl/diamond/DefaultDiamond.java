package md.pixelpainter.game.shape.impl.diamond;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.DefaultShape;

public class DefaultDiamond extends DefaultShape {

    public DefaultDiamond(Size size, Color fillColor, Color outlineColor, int thickness) {
        super(size, fillColor, outlineColor, thickness);
    }

	@Override
	public String toString() {
		return "DefaultDiamond{" +
				"size=" + getSize() +
				", fillColor=" + getFillColor() +
				", outlineColor=" + getOutlineColor() +
				", thickness=" + getThickness() +
				'}';
	}
}
