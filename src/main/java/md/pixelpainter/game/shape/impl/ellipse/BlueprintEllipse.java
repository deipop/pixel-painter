package md.pixelpainter.game.shape.impl.ellipse;

import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.BlueprintShape;

public class BlueprintEllipse extends BlueprintShape {

    public BlueprintEllipse(Size size) {
        super(size);
    }

	@Override
	public String toString() {
		return "BlueprintEllipse{" +
				"size=" + getSize() +
				'}';
	}

}
