package md.pixelpainter.game.shape.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public abstract class DefaultShape implements Shape {

    private final Size size;
    private final Color fillColor;
    private final Color outlineColor;
    private final int thickness;

    public DefaultShape(Size size, Color fillColor, Color outlineColor, int thickness) {
        this.size = size;
        this.fillColor = fillColor;
        this.outlineColor = outlineColor;
        this.thickness = thickness;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public Color getFillColor() {
        return fillColor;
    }

    @Override
    public Color getOutlineColor() {
        return outlineColor;
    }

    @Override
    public int getThickness() {
        return thickness;
    }

    @Override
    public String toString() {
        return "DefaultShape{" +
                "size=" + size +
                ", fillColor=" + fillColor +
                ", outlineColor=" + outlineColor +
                ", thickness=" + thickness +
                '}';
    }
}
