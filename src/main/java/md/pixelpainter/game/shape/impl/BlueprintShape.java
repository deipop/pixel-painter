package md.pixelpainter.game.shape.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public abstract class BlueprintShape implements Shape {

    private static final Color FILL_COLOR = Color.TRANSPARENT;
    private static final Color OUTLINE_COLOR = Color.WHITE;
    private static final int THICKNESS = 2;

    private final Size size;

    public BlueprintShape(Size size) {
        this.size = size;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public Color getFillColor() {
        return FILL_COLOR;
    }

    @Override
    public Color getOutlineColor() {
        return OUTLINE_COLOR;
    }

    @Override
    public int getThickness() {
        return THICKNESS;
    }

    @Override
    public String toString() {
        return "BlueprintShape{" +
                "size=" + size +
                '}';
    }
}
