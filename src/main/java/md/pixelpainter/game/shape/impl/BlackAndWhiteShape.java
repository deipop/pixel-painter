package md.pixelpainter.game.shape.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public abstract class BlackAndWhiteShape implements Shape {

    private static final Color FILL_COLOR = Color.TRANSPARENT;
    private static final Color OUTLINE_COLOR = Color.BLACK;

    private final int thickness;
    private final Size size;

    public BlackAndWhiteShape(int thickness, Size size) {
        this.thickness = thickness;
        this.size = size;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public Color getFillColor() {
        return FILL_COLOR;
    }

    @Override
    public Color getOutlineColor() {
        return OUTLINE_COLOR;
    }

    @Override
    public int getThickness() {
        return thickness;
    }

    @Override
    public String toString() {
        return "BlackAndWhiteShape{" +
                "thickness=" + thickness +
                ", size=" + size +
                '}';
    }
}
