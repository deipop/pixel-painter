package md.pixelpainter.game.shape.impl.ellipse;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.impl.DefaultShape;

public class DefaultEllipse extends DefaultShape {

    public DefaultEllipse(Size size, Color fillColor, Color outlineColor, int thickness) {
        super(size, fillColor, outlineColor, thickness);
    }

	@Override
	public String toString() {
		return "DefaultEllipse{" +
				"size=" + getSize() +
				", fillColor=" + getFillColor() +
				", outlineColor=" + getOutlineColor() +
				", thickness=" + getThickness() +
				'}';
	}
}
