package md.pixelpainter.game.shape.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public class BubbleAdapter implements Shape {

	private final Bubble bubble;

	public BubbleAdapter(Bubble bubble) {
		this.bubble = bubble;
	}

	@Override
	public Size getSize() {
		return new Size(bubble.getSize()[0], bubble.getSize()[1]);
	}

	@Override
	public Color getFillColor() {
		return Color.WHITE;
	}

	@Override
	public Color getOutlineColor() {
		return Color.WHITE;
	}

	@Override
	public int getThickness() {
		return 0;
	}

	@Override
	public String toString() {
		return bubble.toString();
	}

}