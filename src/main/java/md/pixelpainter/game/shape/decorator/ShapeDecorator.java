package md.pixelpainter.game.shape.decorator;

import md.pixelpainter.game.shape.Shape;

public abstract class ShapeDecorator implements Shape {

	protected Shape shape;

	public ShapeDecorator(Shape shape) {
		this.shape = shape;
	}

}