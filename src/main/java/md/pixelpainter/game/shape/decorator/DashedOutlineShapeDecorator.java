package md.pixelpainter.game.shape.decorator;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public class DashedOutlineShapeDecorator extends ShapeDecorator {

	public DashedOutlineShapeDecorator(Shape shape) {
		super(shape);
	}

	@Override
	public Size getSize() {
		return shape.getSize();
	}

	@Override
	public Color getFillColor() {
		return shape.getFillColor();
	}

	@Override
	public Color getOutlineColor() {
		return shape.getOutlineColor();
	}

	@Override
	public int getThickness() {
		return shape.getThickness();
	}

	@Override
	public String toString() {
		return "Dashed " + shape.toString();
	}

}