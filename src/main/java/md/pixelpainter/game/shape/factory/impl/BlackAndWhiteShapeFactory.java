package md.pixelpainter.game.shape.factory.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.criteria.Criteria;
import md.pixelpainter.game.shape.criteria.SizeCriteria;
import md.pixelpainter.game.shape.criteria.ThicknessCriteria;
import md.pixelpainter.game.shape.factory.ShapeFactory;
import md.pixelpainter.game.shape.impl.BlackAndWhiteShape;
import md.pixelpainter.game.shape.impl.Bubble;
import md.pixelpainter.game.shape.impl.BubbleAdapter;
import md.pixelpainter.game.shape.impl.diamond.BlackAndWhiteDiamond;
import md.pixelpainter.game.shape.impl.ellipse.BlackAndWhiteEllipse;
import md.pixelpainter.game.shape.impl.rectangle.BlackAndWhiteRectangle;
import md.pixelpainter.game.util.logger.Logger;

public class BlackAndWhiteShapeFactory implements ShapeFactory {

    private final static Criteria THICKNESS_CRITERIA = new ThicknessCriteria(1, 1);
    private final static Criteria SIZE_CRITERIA = new SizeCriteria(1, 1);

    private boolean meetsCriteria(BlackAndWhiteShape blackAndWhiteShape) {
        if (!SIZE_CRITERIA.meets(blackAndWhiteShape)) {
            Logger.logError("Size criteria is not met");
            return false;
        }
        if (!THICKNESS_CRITERIA.meets(blackAndWhiteShape)) {
            Logger.logError("Thickness criteria is not met");
            return false;
        }
        return true;
    }

    @Override
    public BlackAndWhiteShape createEllipse(Size size, Color fillColor, Color outlineColor, int thickness) {
        BlackAndWhiteShape blackAndWhiteShape = new BlackAndWhiteEllipse(thickness, size);
        return meetsCriteria(blackAndWhiteShape) ? blackAndWhiteShape : null;
    }

    @Override
    public BlackAndWhiteShape createDiamond(Size size, Color fillColor, Color outlineColor, int thickness) {
        BlackAndWhiteShape blackAndWhiteShape = new BlackAndWhiteDiamond(thickness, size);
        return meetsCriteria(blackAndWhiteShape) ? blackAndWhiteShape : null;
    }

    @Override
    public BlackAndWhiteShape createRectangle(Size size, Color fillColor, Color outlineColor, int thickness) {
        BlackAndWhiteShape blackAndWhiteShape = new BlackAndWhiteRectangle(thickness, size);
        return meetsCriteria(blackAndWhiteShape) ? blackAndWhiteShape : null;
    }

	@Override
	public Shape createBubble(Size size) {
		return new BubbleAdapter(new Bubble(new int[] { size.getWidth(), size.getHeight() }));
	}

}