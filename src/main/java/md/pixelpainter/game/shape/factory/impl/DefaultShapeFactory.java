package md.pixelpainter.game.shape.factory.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.criteria.Criteria;
import md.pixelpainter.game.shape.criteria.SizeCriteria;
import md.pixelpainter.game.shape.criteria.ThicknessCriteria;
import md.pixelpainter.game.shape.factory.ShapeFactory;
import md.pixelpainter.game.shape.impl.Bubble;
import md.pixelpainter.game.shape.impl.BubbleAdapter;
import md.pixelpainter.game.shape.impl.DefaultShape;
import md.pixelpainter.game.shape.impl.diamond.DefaultDiamond;
import md.pixelpainter.game.shape.impl.ellipse.DefaultEllipse;
import md.pixelpainter.game.shape.impl.rectangle.DefaultRectangle;
import md.pixelpainter.game.util.logger.Logger;

public class DefaultShapeFactory implements ShapeFactory {

    private final static Criteria THICKNESS_CRITERIA = new ThicknessCriteria(1, 1);
    private final static Criteria SIZE_CRITERIA = new SizeCriteria(1, 1);

    private boolean meetsCriteria(DefaultShape defaultShape) {
        if (!SIZE_CRITERIA.meets(defaultShape)) {
            Logger.logError("Size criteria is not met");
            return false;
        }
        if (!THICKNESS_CRITERIA.meets(defaultShape)) {
            Logger.logError("Thickness criteria is not met");
            return false;
        }
        return true;
    }

    @Override
    public DefaultShape createEllipse(Size size, Color fillColor, Color outlineColor, int thickness) {
        DefaultShape defaultShape = new DefaultEllipse(size, fillColor, outlineColor, thickness);
        return meetsCriteria(defaultShape) ? defaultShape : null;
    }

    @Override
    public DefaultShape createDiamond(Size size, Color fillColor, Color outlineColor, int thickness) {
        DefaultShape defaultShape = new DefaultDiamond(size, fillColor, outlineColor, thickness);
        return meetsCriteria(defaultShape) ? defaultShape : null;
    }

    @Override
    public DefaultShape createRectangle(Size size, Color fillColor, Color outlineColor, int thickness) {
        DefaultShape defaultShape = new DefaultRectangle(size, fillColor, outlineColor, thickness);
        return meetsCriteria(defaultShape) ? defaultShape : null;
    }

	@Override
	public Shape createBubble(Size size) {
		return new BubbleAdapter(new Bubble(new int[] { size.getWidth(), size.getHeight() }));
	}

}