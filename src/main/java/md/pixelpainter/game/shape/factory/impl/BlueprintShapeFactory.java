package md.pixelpainter.game.shape.factory.impl;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;
import md.pixelpainter.game.shape.criteria.Criteria;
import md.pixelpainter.game.shape.criteria.SizeCriteria;
import md.pixelpainter.game.shape.factory.ShapeFactory;
import md.pixelpainter.game.shape.impl.BlueprintShape;
import md.pixelpainter.game.shape.impl.Bubble;
import md.pixelpainter.game.shape.impl.BubbleAdapter;
import md.pixelpainter.game.shape.impl.diamond.BlueprintDiamond;
import md.pixelpainter.game.shape.impl.ellipse.BlueprintEllipse;
import md.pixelpainter.game.shape.impl.rectangle.BlueprintRectangle;
import md.pixelpainter.game.util.logger.Logger;

public class BlueprintShapeFactory implements ShapeFactory {

    private final static Criteria SIZE_CRITERIA = new SizeCriteria(3, 3);

    private boolean meetsCriteria(BlueprintShape blueprintShape) {
        if (!SIZE_CRITERIA.meets(blueprintShape)) {
            Logger.logError("Size criteria is not met");
            return false;
        }
        return true;
    }

    @Override
    public BlueprintShape createEllipse(Size size, Color fillColor, Color outlineColor, int thickness) {
        BlueprintShape blueprintShape = new BlueprintEllipse(size);
        return meetsCriteria(blueprintShape) ? blueprintShape : null;
    }

    @Override
    public BlueprintShape createDiamond(Size size, Color fillColor, Color outlineColor, int thickness) {
        BlueprintShape blueprintShape = new BlueprintDiamond(size);
        return meetsCriteria(blueprintShape) ? blueprintShape : null;
    }

    @Override
    public BlueprintShape createRectangle(Size size, Color fillColor, Color outlineColor, int thickness) {
        BlueprintShape blueprintShape = new BlueprintRectangle(size);
        return meetsCriteria(blueprintShape) ? blueprintShape : null;
    }

	@Override
	public Shape createBubble(Size size) {
		return new BubbleAdapter(new Bubble(new int[] { size.getWidth(), size.getHeight() }));
	}

}