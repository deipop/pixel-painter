package md.pixelpainter.game.shape.factory;

import javafx.scene.paint.Color;
import md.pixelpainter.game.shape.Shape;
import md.pixelpainter.game.shape.Size;

public interface ShapeFactory {

    Shape createEllipse(Size size, Color fillColor, Color outlineColor, int thickness);
    Shape createDiamond(Size size, Color fillColor, Color outlineColor, int thickness);
    Shape createRectangle(Size size, Color fillColor, Color outlineColor, int thickness);
	Shape createBubble(Size size);

}