package md.pixelpainter.game;

import md.pixelpainter.game.character.Character;
import md.pixelpainter.game.character.XYPosition;
import md.pixelpainter.game.command.CommandManager;

public class Game {

    private static Mode mode;

    private final Character character;
    private final CommandManager commandManager;

    public Game(Mode mode) {
        character = new Character(new XYPosition());
        commandManager = new CommandManager(character);
        Game.mode = mode;
    }

    public static Mode getMode() {
        return mode;
    }

    public void parseCommand(String input) {
        commandManager.parse(input);
    }

}