package md.pixelpainter.game;

import md.pixelpainter.game.shape.factory.ShapeFactory;
import md.pixelpainter.game.shape.factory.impl.BlackAndWhiteShapeFactory;
import md.pixelpainter.game.shape.factory.impl.BlueprintShapeFactory;
import md.pixelpainter.game.shape.factory.impl.DefaultShapeFactory;

public enum Mode {
    BLACK_AND_WHITE(new BlackAndWhiteShapeFactory()),
    BLUEPRINT(new BlueprintShapeFactory()),
    DEFAULT(new DefaultShapeFactory());

    private final ShapeFactory shapeFactory;

    Mode(ShapeFactory shapeFactory) {
        this.shapeFactory = shapeFactory;
    }

    public ShapeFactory getShapeFactory() {
        return shapeFactory;
    }
}
