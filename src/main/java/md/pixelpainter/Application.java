package md.pixelpainter;

import md.pixelpainter.game.Game;
import md.pixelpainter.game.Mode;
import md.pixelpainter.game.character.Character;
import md.pixelpainter.game.character.Position;
import md.pixelpainter.game.character.XYPosition;
import md.pixelpainter.game.character.XYZPosition;
import md.pixelpainter.game.util.logger.Logger;

public class Application {

    public static void main(String[] arguments) {
        Logger.logInfo("Mode: " + Mode.BLUEPRINT);
        Game game = new Game(Mode.BLUEPRINT);
        game.parseCommand("draw ellipse 12 22");
        game.parseCommand("draw ellipse 12");
        game.parseCommand("draw ellipse 22 22 #FFFFFF");
        game.parseCommand("draw ellipse 12 22 #AAAA");
        game.parseCommand("draw ellipse 12 32 #AAAAFF #FAFCCA 3");

        Logger.log();
        Logger.log();
        Logger.log();

        Logger.logInfo("Mode: " + Mode.BLACK_AND_WHITE);
        game = new Game(Mode.BLACK_AND_WHITE);
        game.parseCommand("draw ellipse 42 12");
        game.parseCommand("draw ellipse 12");
        game.parseCommand("draw ellipse 52 52 #FFFFFF");
        game.parseCommand("draw ellipse 42 52 #AAAA");
        game.parseCommand("draw ellipse 12 52 #AAAAFF #FAFCCA 3");

	    Logger.log();
	    Logger.log();
	    Logger.log();

	    Logger.logInfo("Mode: " + Mode.DEFAULT);
	    game = new Game(Mode.DEFAULT);
	    game.parseCommand("draw rectangle-dotted 42 52 #AAAAFF #FAFCCA 3");
	    game.parseCommand("draw diamond-dashed 12 52 #AAAAFF #FAFCCA 3");


	    Logger.log();
	    Logger.log();
	    Logger.log();
	    game = new Game(Mode.DEFAULT);
	    game.parseCommand("draw bubble 10 10");

	    Position xyPosition = new XYPosition();
	    Position xyzPosition = new XYZPosition();

	    Character characterXY = new Character(xyPosition);
	    Logger.logInfo(characterXY.toString());

	    Character characterXYZ = new Character(xyzPosition);
	    Logger.logInfo(characterXYZ.toString());

	    game.parseCommand("non_existant_command 50 50 aaaa");
    }

}